package gulshat.android.thermondotestapp.di.module

import dagger.Binds
import dagger.Module
import gulshat.android.thermondotestapp.data.repository.MainRepository
import gulshat.android.thermondotestapp.domain.repository.MainRepositoryImpl

@Module
abstract class DataModule {

    @Binds
    abstract fun main(mainRepositoryImpl: MainRepositoryImpl): MainRepository
}