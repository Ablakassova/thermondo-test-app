package gulshat.android.thermondotestapp.di.module

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import gulshat.android.thermondotestapp.BuildConfig
import gulshat.android.thermondotestapp.data.network.api.MainApi
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val builder = getOkHttpBuilder(30)
        setApiKey(builder)
        setDebugParams(builder)
        return builder.build()
    }

    @Provides
    @Singleton
    internal fun provideMainApi(client: OkHttpClient): MainApi {
        return create(MainApi::class.java, client)
    }

    private fun <T> create(
        service: Class<T>,
        client: OkHttpClient,
        url: String = "https://api.nasa.gov/mars-photos/api/v1/rovers/"
    ): T {
        return Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                        .create()
                )
            )
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(client)
            .build()
            .create(service)
    }

    private fun getOkHttpBuilder(
        timeout: Long,
        builder: OkHttpClient.Builder? = null
    ): OkHttpClient.Builder {
        return builder ?: OkHttpClient().newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
    }

    private fun setDebugParams(builder: OkHttpClient.Builder): OkHttpClient.Builder {
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }
        return builder
    }

    private fun setApiKey(builder: OkHttpClient.Builder): OkHttpClient.Builder {
        builder.addInterceptor {
            val original = it.request()
            val url = original.url().newBuilder()
                .addQueryParameter("api_key", BuildConfig.API_KEY)
                .build()

            val requestBuilder: Request.Builder = original.newBuilder()
                .url(url)
            it.proceed(requestBuilder.build())
        }
        return builder
    }
}