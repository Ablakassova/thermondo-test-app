package gulshat.android.thermondotestapp.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import gulshat.android.thermondotestapp.ui.main.MainActivity

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun bindRoot(): MainActivity
}