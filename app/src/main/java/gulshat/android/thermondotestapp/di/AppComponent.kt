package gulshat.android.thermondotestapp.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import gulshat.android.thermondotestapp.App
import gulshat.android.thermondotestapp.di.module.ActivityBindingModule
import gulshat.android.thermondotestapp.di.module.ApplicationModule
import gulshat.android.thermondotestapp.di.module.NetworkModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApplicationModule::class,
        ActivityBindingModule::class,
        NetworkModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: App): Builder

        fun build(): ApplicationComponent
    }
}