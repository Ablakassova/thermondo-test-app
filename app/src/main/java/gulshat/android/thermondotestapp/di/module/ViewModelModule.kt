package gulshat.android.thermondotestapp.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import gulshat.android.thermondotestapp.di.viewModel.ViewModelFactory
import gulshat.android.thermondotestapp.di.module.fragments.MainViewModelModule

@Module(includes = [MainViewModelModule::class])
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}