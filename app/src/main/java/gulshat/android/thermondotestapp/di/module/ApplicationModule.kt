package gulshat.android.thermondotestapp.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import gulshat.android.thermondotestapp.App
import gulshat.android.thermondotestapp.ContextProvider
import gulshat.android.thermondotestapp.di.viewModel.ContextProviderImpl
import javax.inject.Singleton


@Module(
    includes = [
        DataModule::class,
        ViewModelModule::class
    ]
)
class ApplicationModule {

    @Provides
    @Singleton
    internal fun provideApplicationContext(app: App): Context {
        return app
    }

    @Provides
    @Singleton
    internal fun provideContextProvider(contextProviderImpl: ContextProviderImpl): ContextProvider =
        contextProviderImpl
}