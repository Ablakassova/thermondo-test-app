package gulshat.android.thermondotestapp

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import gulshat.android.thermondotestapp.di.DaggerApplicationComponent

class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder().application(this).build()
    }
}