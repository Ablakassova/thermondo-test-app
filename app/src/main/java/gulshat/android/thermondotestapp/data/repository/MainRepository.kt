package gulshat.android.thermondotestapp.data.repository

import gulshat.android.thermondotestapp.data.network.Response
import gulshat.android.thermondotestapp.domain.Photo

interface MainRepository {

    suspend fun getPhotos(roverName: String?, earthDate: String): Response<List<Photo>>
}