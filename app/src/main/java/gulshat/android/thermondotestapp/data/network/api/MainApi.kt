package gulshat.android.thermondotestapp.data.network.api

import gulshat.android.thermondotestapp.domain.GetPhotosResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MainApi {

    @GET("{roverName}/photos")
    fun getPhotos(@Path("roverName") roverName: String?, @Query("earth_date") earthDate: String): Deferred<GetPhotosResponse>
}