package gulshat.android.thermondotestapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import gulshat.android.thermondotestapp.BaseViewModel
import gulshat.android.thermondotestapp.ContextProvider
import gulshat.android.thermondotestapp.data.network.Response
import gulshat.android.thermondotestapp.domain.Photo
import gulshat.android.thermondotestapp.domain.interactor.GetPhotosUseCase
import gulshat.android.thermondotestapp.domain.interactor.GetPhotosUseCaseParams
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getPhotosUseCase: GetPhotosUseCase,
    contextProvider: ContextProvider
) : BaseViewModel(contextProvider) {

    private var photosMutableLiveData = MutableLiveData<Response<List<Photo>?>>()

    val photosLiveData: LiveData<Response<List<Photo>?>>
        get() = photosMutableLiveData

    fun getPhotos(forceUpdate: Boolean = false, roverName: String?) {
        photosMutableLiveData.value = Response.Loading
        launch {
            photosMutableLiveData.value = getPhotosUseCase(
                GetPhotosUseCaseParams(
                    roverName,
                    "2015-6-1"
                )
            )
        }
    }
}