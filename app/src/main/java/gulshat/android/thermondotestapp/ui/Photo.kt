package gulshat.android.thermondotestapp.ui

enum class RoverName(val value: String) {
    CURIOSITY("curiosity"),
    OPPORTUNITY("opportunity"),
    SPIRIT("spirit")
}