package gulshat.android.thermondotestapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import gulshat.android.thermondotestapp.R
import gulshat.android.thermondotestapp.domain.Photo
import kotlinx.android.synthetic.main.fragment_details_bottom_sheet.*

class DetailsBottomSheetFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<Photo>(PHOTO)?.let {
            roverNameTextView.text = it.rover?.name
            launchDateNameTextView.text = it.rover?.getLaunchDateString()
            landingDateTextView.text = it.rover?.getLandingDateString()
        }
    }

    companion object {

        private const val PHOTO = "photo"

        fun newInstance(photo: Photo) = DetailsBottomSheetFragment().apply {
            arguments = bundleOf(Pair(PHOTO, photo))
        }
    }
}