package gulshat.android.thermondotestapp.ui

import android.widget.ImageView

fun ImageView.load(url: String?) {
    PicassoAllCertTrust.getInstance(context).load(url).into(this)
}