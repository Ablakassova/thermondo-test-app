package gulshat.android.thermondotestapp.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import gulshat.android.thermondotestapp.R
import gulshat.android.thermondotestapp.domain.Photo
import gulshat.android.thermondotestapp.ui.load
import kotlinx.android.synthetic.main.item_photo.view.*

class PhotoAdapter(
    private val onClickListener: (Photo) -> Unit,
    private val bottomReachListener: (Unit) -> Unit
) : ListAdapter<Photo, ViewHolder>(PhotoDiffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false),
            onClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == itemCount - 1) {
            bottomReachListener(Unit)
        }
        holder.bind(getItem(position))
    }
}

class ViewHolder(
    view: View,
    private val onItemClickListener: (Photo) -> Unit
) : RecyclerView.ViewHolder(view) {

    private var photo: Photo? = null

    init {
        itemView.setOnClickListener {
            photo?.let {
                onItemClickListener(it)
            }
        }
    }

    fun bind(photo: Photo) {
        this.photo = photo
        itemView.apply {
            roverNameTextView.text = photo.rover?.name
            imageView.load(photo.imgSrc)
            cameraTextView.text = photo.camera?.name
            dateTextView.text = photo.getDate()
        }
    }
}

object PhotoDiffUtil : DiffUtil.ItemCallback<Photo>() {
    override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
        return oldItem.areContentsTheSame(newItem)
    }
}