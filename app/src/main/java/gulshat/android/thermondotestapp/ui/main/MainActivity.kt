package gulshat.android.thermondotestapp.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ProgressBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import gulshat.android.thermondotestapp.R
import gulshat.android.thermondotestapp.data.network.Response
import gulshat.android.thermondotestapp.domain.Photo
import gulshat.android.thermondotestapp.ui.RoverName
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var progressBar: ProgressBar

    private var currentRoverName: String? = null

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.overflowIcon =
            ContextCompat.getDrawable(applicationContext, R.drawable.ic_filter_list_black_24dp)
        setSupportActionBar(toolbar)
        initView()
        initObservables()
        currentRoverName = RoverName.CURIOSITY.value
        mainViewModel.getPhotos(roverName = currentRoverName)
    }

    override fun onRefresh() {
        recyclerView.adapter = PhotoAdapter(onItemClickListener, bottomReachListener)
        mainViewModel.getPhotos(forceUpdate = true, roverName = currentRoverName)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_rovers, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        currentRoverName = when (item.itemId) {
            R.id.curiosity -> RoverName.CURIOSITY.value
            R.id.spirit -> RoverName.SPIRIT.value
            R.id.opportunity -> RoverName.OPPORTUNITY.value
            else -> null
        }
        mainViewModel.getPhotos(roverName = currentRoverName)
        return super.onOptionsItemSelected(item)
    }

    private fun handleLoading(isLoading: Boolean) {
        if (swipeRefreshLayout.isRefreshing) swipeRefreshLayout.isRefreshing = false
        else {
            progressBar.isVisible = isLoading
            recyclerView.isVisible = !isLoading
        }
    }

    private fun onError(error: Response.Error) {
        val message = error.apiErrorMessage
            ?: getString(
                when {
                    error.isNetworkError -> R.string.network_error
                    error.isHttpException -> R.string.http_error
                    else -> R.string.unknown_error
                }
            )
        Snackbar.make(container, message, Snackbar.LENGTH_SHORT).show()
        R.id.curiosity
    }

    private fun initObservables() {
        mainViewModel.photosLiveData.observe(this, Observer {
            handleLoading(isLoading = false)
            when (it) {
                is Response.Success -> {
                    recyclerView.adapter = PhotoAdapter(
                        onItemClickListener,
                        bottomReachListener
                    ).apply { submitList(it.data) }
                }
                is Response.Loading -> {
                    handleLoading(isLoading = true)
                }
                is Response.Error -> {
                    onError(it)
                }
            }
        })
    }

    private fun initView() {
        val verticalDividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            LinearLayoutManager.VERTICAL
        )
        recyclerView.apply {
            addItemDecoration(verticalDividerItemDecoration)
        }
        swipeRefreshLayout.setOnRefreshListener(this)
        progressBar = centerProgressBar
    }

    private val onItemClickListener: (Photo) -> Unit = {
        DetailsBottomSheetFragment.newInstance(it)
            .show(supportFragmentManager, this::class.java.simpleName)
    }

    private val bottomReachListener: (Unit) -> Unit = {
    }
}
