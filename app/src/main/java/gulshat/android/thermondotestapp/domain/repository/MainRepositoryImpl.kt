package gulshat.android.thermondotestapp.domain.repository

import gulshat.android.thermondotestapp.data.network.Response
import gulshat.android.thermondotestapp.data.network.api.MainApi
import gulshat.android.thermondotestapp.data.repository.MainRepository
import gulshat.android.thermondotestapp.domain.Photo
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val mainApi: MainApi
) : MainRepository {

    override suspend fun getPhotos(roverName: String?, earthDate: String): Response<List<Photo>> {
        return try {
            val response = mainApi.getPhotos(roverName, earthDate).await()
            if (response.error == null && response.photos != null) {
                Response.Success(response.photos)
            } else {
                Response.Error(apiErrorMessage = response.error)
            }
        } catch (exception: Exception) {
            Response.Error(exception)
        }
    }
}