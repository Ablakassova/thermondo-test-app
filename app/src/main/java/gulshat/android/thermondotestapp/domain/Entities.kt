package gulshat.android.thermondotestapp.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Photo(
    val id: Int?,
    val sol: Int?,
    val imgSrc: String?,
    val camera: Camera?,
    val rover: Rover?,
    val earthDate: String?
) : Parcelable {

    fun areContentsTheSame(newItem: Photo): Boolean {
        return this.imgSrc == newItem.imgSrc
                && this.earthDate == newItem.earthDate
                && this.rover?.name == newItem.rover?.name
                && this.camera?.name == newItem.camera?.name
    }

    fun getDate(): String? {
        if (earthDate == null)
            return earthDate
        val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(earthDate)
        return if (date == null) date else SimpleDateFormat(
            "dd MMM yyyy",
            Locale.getDefault()
        ).format(date)
    }
}

@Parcelize
data class Camera(
    val id: Int?,
    val name: String?,
    val roverId: Int?,
    val fullName: String?
) : Parcelable

@Parcelize
data class Rover(
    val id: Int?,
    val name: String?,
    val landingDate: String?,
    val launchDate: String?,
    val status: String?,
    val cameras: List<Camera>?
) : Parcelable {

    fun getLandingDateString(): String? {
        if (landingDate == null)
            return landingDate
        return getDate(landingDate)
    }

    fun getLaunchDateString(): String? {
        if (launchDate == null)
            return launchDate
        return getDate(launchDate)
    }

    private fun getDate(dateString: String): String? {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val format = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
        val date = dateFormat.parse(dateString)
        return if (date == null) date else format.format(date)
    }
}

data class GetPhotosResponse(
    val photos: List<Photo>?,
    val error: String?
)