package gulshat.android.thermondotestapp.domain.interactor

import gulshat.android.thermondotestapp.domain.repository.MainRepositoryImpl
import gulshat.android.thermondotestapp.data.network.Response
import gulshat.android.thermondotestapp.domain.Photo
import javax.inject.Inject

class GetPhotosUseCase @Inject constructor(
    private val mainRepository: MainRepositoryImpl
) : UseCase<GetPhotosUseCaseParams, Response<List<Photo>>>() {

    override suspend fun execute(params: GetPhotosUseCaseParams): Response<List<Photo>> {
        return mainRepository.getPhotos(params.roverName, params.earthDate)
    }
}

data class GetPhotosUseCaseParams(
    val roverName: String?,
    val earthDate: String
)