package gulshat.android.thermondotestapp

import kotlin.coroutines.CoroutineContext

interface ContextProvider {
    val ui: CoroutineContext
}